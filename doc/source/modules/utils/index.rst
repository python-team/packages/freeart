utils
=====

.. currentmodule:: freeart.utils

:mod:`freeart.utils` 
--------------------

.. automodule:: freeart.utils

:mod:`freeart.utils.reconstrutils`: some utils linked to the reconstruction
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

.. currentmodule:: freeart.utils.reconstrutils
.. automodule:: freeart.utils.reconstrutils
    :members: decreaseMatSize, increaseMatSize, savePhantom, LoadEdf_2D, makeFreeARTFluoSinogram, convertToFisxMaterial


:mod:`freeart.utils.genph`: phantom generator
"""""""""""""""""""""""""""""""""""""""""""""

.. currentmodule:: freeart.utils.genph
.. autoclass:: PhantomGenerator
   :show-inheritance:
   :members: 

:mod:`freeart.utils.benchmark`: tool for benchmarking
"""""""""""""""""""""""""""""""""""""""""""""""""""""

.. currentmodule:: freeart.utils.benchmark
.. automodule:: freeart.utils.benchmark
    :members: 
