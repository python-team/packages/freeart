
.. currentmodule:: freeart.configuration

:mod:`configuration`: Used by the interpreter to know about reconstructions to run
----------------------------------------------------------------------------------

.. automodule:: freeart.configuration
   :members: read, save


.. toctree::
   :maxdepth: 1

   config.rst
   fileInfo.rst
   structs.rst