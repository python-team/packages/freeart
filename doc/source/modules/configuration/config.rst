
.. currentmodule:: freeart.configuration.config

:mod:`config`: define classes containing needed information to run reconstructions
----------------------------------------------------------------------------------

.. automodule:: freeart.configuration.config
   :members:

