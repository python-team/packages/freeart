
.. currentmodule:: freeart.configuration.structs

:mod:`structs`: IPython console widgets
---------------------------------------

.. automodule:: freeart.configuration.structs
   :members:
