
freeart
#######

freeart is a tomographic image reconstruction library using Algebraic Reconstruction Technique (ART).
It is able to deal with absorption and emission sinograms.
Freeart core is written in C++ using template classes to have simple or double precision.
A python interface is provided to access the core functionalities.
A set of utils such as configuration files, interpreter to run reconstruction from configuration files are provided in python.


Installation
------------

To install freeart, run:

.. code-block:: python

    pip install freeart

Then you can use freeart module and submodules from python.

Documentation
-------------

Documentation of latest release is available at http://www.edna-site.org/pub/doc/freeart/latest/index.html

GUI
---

tomogui (https://pypi.org/project/tomogui/ and https://gitlab.esrf.fr/tomoTools/tomogui) is providing a graphical user interface to run freeart reconstruction.

:Authors: - Nicola Vigano
          - Emmanuel Taurel
          - Henri PAYNO