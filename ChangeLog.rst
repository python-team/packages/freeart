Change Log
==========

3.3.0:
------

- use silx.io.url instead of the internal system.


3.2.0: 2018/03/16
-----------------

- add the configuration module
- remove the specific case of compton from API
- fix bug with fisx: create a unique material name
- create automatically unique sinogram names
- deal with h5 files
- fix unit micrometer (was wrong before - typo)
- add continuous integration
- add examples for fluorescence and transmission
- add precision management from the interpreter

3.1.0: 2017/09/21
-----------------

- version to test on line
- add the interpreter module
- add metric system
- compatibility python2 - python3

3.0.0:
------

- rework on structure
- add unit test
- use the silx setup system
- manage to have the same behavior between V1.0 and V2.0
